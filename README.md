# Descripcion
 Esta pagina web calcula una tarifa fija por mes, dividiendo el monto total y el plazo, como se indico en el ejercicio, se puede ingresar un número usando el slider o escribiendo manualmente el input y  solo se permiten números.

## Librerias Usadas
* rc-slider 
* Material UI
### Resultado
![UI](/Resultado.png?raw=true)

### Implementación
1. Primero se hizo el maquetado, dividiendo en componentes pequeños, y buscar reutilizar los mismos.
2. Despues se procedio a implementar las funcionalidades, y hacer la sincronizacion entre el input y el slider.
3. Por ultimo se calculo la cuota fija por mes y se agrego en el caso q fuera necesario los simbolos pesos y la separacion de mil.

````

### Instalación

1. Descargar el codigo de Github con la opción de descarga o hacer un fork del repositorio (a gusto). 
2. `yarn` o `npm install` para instalar paquetes npm 
3. `yarn start` o `npm start` para inicializar server de desarrollo