import { Button } from "@mui/material";
import React from "react";
const ComponentButton = ({ variant, text, name }) => {
  return (
    <>
      <Button
        fullWidth
        sx={{ mt: 0, mb: 1 }}
        variant={variant}
        style={
          name === "buttonCred"
            ? {
                height: "70%",
                background: "#17aa8d",
                fontSize: "1.3rem",
                fontWeight: "bold",
              }
            : {
                height: "70%",
                background: "#074a7e",
                fontSize: "1rem",
                fontWeight: "bold",
              }
        }
      >
        {text}
      </Button>
    </>
  );
};
export default ComponentButton;
