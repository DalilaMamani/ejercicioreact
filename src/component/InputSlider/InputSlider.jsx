import React, { useEffect, useRef } from "react";
import { ComponentInput } from "../ComponentInput/ComponentInput";
import { ComponentSlider } from "../ComponentSlider/ComponentSlider";

export const InputSlider = ({ text, value, min, max, currency, onChange }) => {
  const inputRef = useRef(null);
  useEffect(() => {
    inputRef.current.value = min;
  }, []);
  const handleOnChangeInput = (val) => {
    let aux = val;
    if (currency) {
      val = (val + "").replace("$", "").replace(",", "");
    }
    /* if (!/^\d+$/.test(val)) {
      aux = min;
    } */
    if (val < min) {
      aux = min;
    } else if (val > max) {
      aux = max;
    }
    inputRef.current.value = getCurrency(aux);

    onChange(+aux);
  };
  const getCurrency = (val) => {
    return currency
      ? "$" + (val + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
      : val;
  };
  const debounce = (func, delay) => {
    let debounceTimer;
    return function () {
      const context = this;
      const args = arguments;
      clearTimeout(debounceTimer);
      debounceTimer = setTimeout(() => func.apply(context, args), delay);
    };
  };

  const debounceValue = debounce((e) => {
    handleOnChangeInput(e.target.value);
  }, 1000);

  return (
    <>
      <div className="block">
        <ComponentInput
          text={text}
          value={value}
          min={min}
          max={max}
          type="number"
          currency={true}
          handleInputChange={debounceValue}
          ref={inputRef}
        />
        <ComponentSlider
          min={min}
          max={max}
          currency={currency}
          value={value}
          onChange={(e) => handleOnChangeInput(e)}
        />
      </div>
    </>
  );
};
