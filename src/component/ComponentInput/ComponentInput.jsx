import { Box } from "@mui/system";
import React from "react";
import "../../style/style.css";

export const ComponentInput = React.forwardRef(
  ({ name, min, max, text, handleInputChange }, ref) => {
    return (
      <Box className="componentInput">
        <label className="label">{text}</label>
        <input
          name={name}
          onChange={handleInputChange}
          ref={ref}
          min={min}
          max={max}
          className="input"
        />
      </Box>
    );
  }
);
