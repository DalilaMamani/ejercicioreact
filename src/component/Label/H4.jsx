import { Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React from "react";
const useStyles = makeStyles(() => {
  return {
    label: {
      float: "right",
      fontWeight: "bold",
      fontFamily: "'Montserrat', sans-serif",
      paddingRight: 15,
    },
  };
});
export const H4 = (props) => {
  const classes = useStyles();
  return (
    <Typography
      variant="h4"
      className={classes.label}
      style={{ fontWeight: "bold", fontFamily: "'Montserrat', sans-serif" }}
    >
      {props.children}
    </Typography>
  );
};
