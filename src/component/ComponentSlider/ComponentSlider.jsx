import React from "react";
import Slider from "rc-slider";
import "rc-slider/assets/index.css";

const MySlider = Slider.createSliderWithTooltip(Slider);

export const ComponentSlider = ({ min, max, value, currency, onChange }) => {
  return (
    <div>
      <MySlider
        min={min}
        max={max}
        value={value}
        onChange={onChange}
        className="mySlider"
      />
      <div>
        <label className="labelSliderMin">
          {currency && "$"}
          {min}
        </label>
        <label className="labelSliderMax">
          {currency && "$"}
          {max}
        </label>
      </div>
    </div>
  );
};
