import React from "react";
import { Typography } from "@mui/material";
import { Box } from "@mui/system";

const Title = ({ text }) => {
  return (
    <Typography component="div">
      <Box className="title">{text}</Box>
    </Typography>
  );
};
export default Title;
