import React, { useContext, useEffect, useState } from "react";
import Title from "../../component/Title/Title";
import { Box } from "@mui/system";
import { makeStyles } from "@mui/styles";
import ComponentButton from "../../component/ComponentButton/ComponentButton";
import { Grid, Stack } from "@mui/material";
import "../../style/style.css";
import { Context } from "../../component/Context/Context";
import { H4 } from "../../component/Label/H4";
import { InputSlider } from "../../component/InputSlider/InputSlider";

const useStyles = makeStyles(() => {
  return {
    box: {
      fontWeight: "bold",
      fontSize: 15,
      padding: 5,
      margin: "10 10 0 10",
      background: "#00355d",
      height: "60px",
      justifyContent: "center",
      textAlign: "center",
    },

    label: {
      float: "left",
      paddingLeft: 15,
      paddingTop: 20,
    },
  };
});
export const CreditSimulation = () => {
  const { totalAmount, setTotalAmount, term, setTerm } = useContext(Context);
  const classes = useStyles();
  const [total, setTotal] = useState(0);

  /* useEffect(() => {
    setTimeout(calcTotal, 1000);
  }, []); */

  const calcTotal = (a, b) => {
    let ultimoTotal = a / b;
    setTotal(ultimoTotal);
  };

  const getCurrency = (val) => {
    return (
      "$ " + ("" + val.toFixed(2)).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    );
  };
  const handleInputTotalAmount = (event) => {
    setTotalAmount(event);
    calcTotal(event, term);
  };
  const handleInputTerm = (event) => {
    setTerm(event);
    calcTotal(totalAmount, event);
  };
  return (
    <div className="conteiner">
      <Title text="Simulá tu crédito" />
      <div className="body">
        <Box className="boxTop">
          <div className="block">
            <InputSlider
              text="MONTO TOTAL"
              name="totalAmount"
              value={totalAmount}
              min={5000}
              max={50000}
              currency={true}
              onChange={handleInputTotalAmount}
            />
          </div>
          <div className="block">
            <InputSlider
              text="PLAZO"
              name="term"
              value={term}
              min={3}
              max={24}
              type="number"
              currency={false}
              onChange={handleInputTerm}
            />
          </div>
        </Box>
        <Box className={classes.box}>
          <label className={classes.label}>CUOTA FIJA POR MES</label>
          <H4>{getCurrency(total)}</H4>
        </Box>
        <Stack direction="row" spacing={2} className="groupButton">
          <Grid item xs={6} md={7}>
            <ComponentButton
              name="buttonCred"
              variant="contained"
              text="OBTENÉ CRÉDITO"
              fullWidth
              className="buttonCred"
            />
          </Grid>
          <Grid item xs={6} md={5}>
            <ComponentButton
              name="buttonShare"
              variant="contained"
              text="VER DETALLE DE CUOTAS"
            />
          </Grid>
        </Stack>
      </div>
    </div>
  );
};
