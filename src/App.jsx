import { useState } from "react";
import "./App.css";
import { Context } from "./component/Context/Context";
import { CreditSimulation } from "./views/CreditSimulation/CreditSimulation";

function App() {
  const [totalAmount, setTotalAmount] = useState(5000);
  const [term, setTerm] = useState(3);
  return (
    <div className="App">
      <Context.Provider value={{ totalAmount, setTotalAmount, term, setTerm }}>
        <CreditSimulation />
      </Context.Provider>
    </div>
  );
}

export default App;
